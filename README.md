# Shifts in carbon partitioning by photosynthetic activity increase terpenoid synthesis in glandular trichomes

Authors:

- Nima P. Saadat <a href="https://orcid.org/0000-0002-1262-6591"><img src="https://orcid.org/assets/vectors/orcid.logo.icon.svg" width=20></a>
- Marvin van Aalst <a href="https://orcid.org/0000-0002-7434-0249"><img src="https://orcid.org/assets/vectors/orcid.logo.icon.svg" width=20></a>
- Alejandro Brand <a href="https://orcid.org/0000-0002-6896-4995"><img src="https://orcid.org/assets/vectors/orcid.logo.icon.svg" width=20></a>
- Oliver Ebenhöh <a href="https://orcid.org/0000-0002-7229-7398"><img src="https://orcid.org/assets/vectors/orcid.logo.icon.svg" width=20></a>
- Alain Tissier <a href="https://orcid.org/0000-0002-9406-4245"><img src="https://orcid.org/assets/vectors/orcid.logo.icon.svg" width=20></a>
- Anna B. Matuszyńska <a href="https://orcid.org/0000-0003-0882-6088"><img src="https://orcid.org/assets/vectors/orcid.logo.icon.svg" width=20></a>

## Installation and running of scripts

- Install a Python version `>= 3.10` and some form of running jupyter notebooks, e.g. [jupyter lab/notebook](https://jupyter.org/) or [vscode](https://code.visualstudio.com/) with the [jupyter extension](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter)
- Install all dependencies via `pip install -r requirements.txt`
- Download required data
  - metacyc version `25.0` from [SIR International](http://bioinformatics.ai.sri.com/)
  - LycoCyc version `3.3` from [Solgenomics](https://solcyc.solgenomics.net/organism-summary?object=LYCO)
  - Download the transcriptomic results (supplementary dataset 5) from [Balcke 2017](https://www.doi.org/10.1105/tpc.17.00060)
- Either put the downloaded files in a folder called `data` at the root of this project or change the paths to the downloaded files (first notebook cell in both notebooks) to your respective paths
- Run the remaining cells in the `analysis.ipynb` notebook to repeat the published figures

